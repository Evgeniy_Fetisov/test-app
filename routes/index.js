var express = require('express');
var router = express.Router();

const data = [
  {id: 1, value: 'one'},
  {id: 2, value: 'two'},
  {id: 3, value: 'three'},
  {id: 4, value: 'four'},
  {id: 5, value: 'five'}
];


router.get('/solution', function(req, res, next) {
  res.render('solution', { data });
});

router.get('/api/solution', function(req, res, next) {
  res.json(data);
});


module.exports = router;
